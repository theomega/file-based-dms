#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:tw=80:ts=2:sw=2:colorcolumn=81:smartindent:expandtab
# pylint: disable=missing-docstring

import argparse
import configparser
import datetime
import logging
import os
import sys

import file_handling
import tag_handling
import utils


def get_information(
    date_format, title_history_file, tags_history_file, date_history_file, tags_file
):

    tag_options = tag_handling.read_tags(tags_file)
    logging.debug("Tag options are %s", tag_options)

    # Title
    title = utils.get_prompt(
        "Enter Title: ", title_history_file, None, utils.get_validate_title()
    )

    # Tags
    tags_str = utils.get_prompt(
        "Enter Tags: ", tags_history_file, tag_options, utils.get_validate_tags()
    )
    if tags_str:
        tags = tags_str.split(" ")
    else:
        tags = []

    # Date
    now = datetime.datetime.now().strftime(date_format)
    date = utils.get_prompt(
        "Enter date: ",
        date_history_file,
        None,
        utils.get_validate_date(date_format),
        now,
    )
    date = datetime.datetime.strptime(date, date_format)

    logging.debug('Got title="%s", tags="%s", date="%s"', title, tags, date)

    # Saving Tags
    tag_options.update(tags)
    tag_handling.write_tags(tags_file, tag_options)

    return (title, tags, date)


def main(argv):
    cmd = argparse.ArgumentParser(description="Add a new document")
    cmd.add_argument(
        "-c",
        "--config",
        help="configuration file",
        required=True,
        type=utils.validate_file,
    )
    cmd.add_argument(
        "-d",
        "--debug",
        action="store_true",
        default=False,
        help="show debugging output",
    )
    cmd.add_argument(
        "-r",
        "--rm",
        action="store_true",
        default=False,
        help="Remove the original file",
    )
    cmd.add_argument(
        "--forceocr",
        action="store_true",
        default=False,
        help="Force OCRing the file, even it it already contains a text layer. "
        + "This can mainly help in case of PDFs which are corrupt as it "
        + "rewrites the PDF",
    )
    cmd.add_argument(
        "-m",
        "--mode",
        choices=["single", "join", "enum"],
        default="single",
        help="Mode to apply when adding multiple files",
    )
    cmd.add_argument(
        "filename", nargs="+", type=utils.validate_file, help="file to add"
    )
    args = cmd.parse_args(argv[1:])

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    config_parser = configparser.RawConfigParser()
    config_parser.read(args.config)
    config = dict(config_parser["Defaults"])
    logging.debug("Config is %s", config)

    target_folder = config["target_folder"]

    if args.mode == "single" or len(args.filename) == 1:
        # In single mode, ask for each file for information and put each file
        # into its own file
        for org_file in args.filename:
            (title, tags, date) = get_information(
                date_format=config["date_format"],
                title_history_file=config["title_history_file"],
                tags_history_file=config["tags_history_file"],
                date_history_file=config["date_history_file"],
                tags_file=config["tags_file"],
            )

            filepath = file_handling.get_filepath(
                title, tags, date, config["date_format"], target_folder
            )
            logging.debug("Computed Filepath %s", filepath)

            file_handling.ocr_file(
                input_file=org_file,
                output_file=filepath,
                language=config["language"],
                temp_dir=config.get("temp_dir", None),
                force_ocr=args.forceocr,
            )

            print("-> %s" % (filepath))

    elif args.mode == "enum":
        # In enum-mode, ask only once for information and enumerate the files (1, 2,
        # 3)
        print("Enumeration Mode for:")
        for org_file in args.filename:
            print("  - %s" % (os.path.basename(org_file)))

        (title, tags, date) = get_information(
            date_format=config["date_format"],
            title_history_file=config["title_history_file"],
            tags_history_file=config["tags_history_file"],
            date_history_file=config["date_history_file"],
            tags_file=config["tags_file"],
        )

        for (i, org_file) in enumerate(args.filename):
            filepath = file_handling.get_filepath(
                title, tags, date, config["date_format"], target_folder, i
            )
            logging.debug("Computed Filepath %s", filepath)

            file_handling.ocr_file(
                input_file=org_file,
                output_file=filepath,
                language=config["language"],
                temp_dir=config.get("temp_dir", None),
                force_ocr=args.forceocr,
            )

            print("-> %s" % (filepath))

    elif args.mode == "join":
        # In join-mode, ask only once for the information. Join all the PDFs into
        # a single one and store the joined file

        print("Join Mode for:")
        for org_file in args.filename:
            print("  - %s" % (os.path.basename(org_file)))

        (title, tags, date) = get_information(
            date_format=config["date_format"],
            title_history_file=config["title_history_file"],
            tags_history_file=config["tags_history_file"],
            date_history_file=config["date_history_file"],
            tags_file=config["tags_file"],
        )

        filepath = file_handling.get_filepath(
            title, tags, date, config["date_format"], target_folder
        )
        logging.debug("Computed Filename %s", filepath)

        # Join File together
        file_handling.join_files(
            input_files=args.filename,
            output_file=filepath,
            temp_dir=config.get("temp_dir", None),
        )

        # Run OCR
        file_handling.ocr_file(
            input_file=filepath,
            output_file=filepath,
            language=config["language"],
            temp_dir=config.get("temp_dir", None),
            force_ocr=args.forceocr,
        )

    # No matter which mode, after success, remove files if requested to do so
    if args.rm:
        for org_file in args.filename:
            logging.debug("Deleting %s", org_file)
            os.remove(org_file)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
