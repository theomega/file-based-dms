#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:tw=80:ts=2:sw=2:colorcolumn=81:smartindent:expandtab
# pylint: disable=missing-docstring

import logging
import os
import string
import subprocess
import tempfile
from shutil import copyfile


def get_filepath(title, tags, date, date_format, target_folder, index=None):
    # Add Date and Title
    filename = "%s - %s" % (date.strftime(date_format), title)

    if tags:
        # If there are tags to add, add them
        filename += " %s" % ("".join(["[%s]" % t for t in tags]))

    if index is not None:
        # Add index if required
        filename += " - %02d" % (index)

    # add file extension
    filename += ".pdf"

    doc_folder = date.strftime(target_folder)
    os.makedirs(doc_folder, exist_ok=True)

    return os.path.join(doc_folder, filename)


# pylint: disable=too-many-arguments
def ocr_file(
    input_file, output_file, language, temp_dir=None, output_type=None, force_ocr=False
):
    with tempfile.NamedTemporaryFile(dir=temp_dir) as temp_output:
        # Run OCRMyPDF
        logging.debug(
            "Running OCRMyPDF on input %s, output %s, temp output %s",
            input_file,
            output_file,
            temp_output.name,
        )

        params = ["-l", language, input_file, temp_output.name]
        if force_ocr:
            # If there is already some text in the PDF, still do the OCR
            params = ["--force-ocr"] + params
        else:
            # If there is already some text in the PDF (i.e. internet generated
            # then skip the OCRing
            params = ["--skip-text"] + params

        if output_type:
            params = ["--output-type", output_type] + params

        try:
            c = subprocess.run(["ocrmypdf"] + params, check=True, capture_output=True)
        except subprocess.CalledProcessError as e:
            logging.error(e.output.decode())
            logging.error(e.stderr.decode())
            raise e

        logging.debug("Copying %s to %s", temp_output.name, output_file)
        copyfile(temp_output.name, output_file)


def join_files(input_files, output_file, temp_dir=None):
    # Run PDF TK
    logging.debug("Joining together files %s, output %s", input_files, output_file)

    params = input_files.copy()

    params += ["cat", "output", output_file]
    c = subprocess.run(["pdftk"] + params, check=True, capture_output=True)
    logging.debug("Joining together ok, results in %s", output_file)


def get_valid_filename_chars():
    return string.ascii_letters + string.digits + " '.+!@#$%^&()-_,;üÜöÖäÄß€"
