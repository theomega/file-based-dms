#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:tw=80:ts=2:sw=2:colorcolumn=81:smartindent:expandtab
"""Helper Functions for handling tags"""

import logging
import os


def read_tags(tags_file):
    """Reads the available tags from the provided file and returns them. If the
    file does not exist or is not a file, an empty set is returned.

    Args:
       tags_file (string): The filename to read the tags from. If None, an empty
                           set of tags is returned.

    Returns:
       set: A set of tags
    """
    logging.debug("Reading tags from %s", tags_file)
    if tags_file is None:
        return []

    tags_path = os.path.expanduser(tags_file)
    if os.path.isfile(tags_path):
        with open(tags_path, "r") as f:
            return {x.strip() for x in f.readlines()}
    else:
        return set()


def write_tags(tags_file, tags):
    """Writes the provided tags to the provided filename. Makes sure
    that the folder where the file should reside in exists.

    Args:
       tags_file (string): The filename to write the tags to
       tags (iterable): The list/set of tags
    """
    logging.debug("Saving tags to %s", tags_file)
    if tags_file is None:
        return

    tags_path = os.path.expanduser(tags_file)
    tags_folder = os.path.dirname(tags_path)
    if not os.path.exists(tags_folder):
        os.makedirs(tags_folder)

    with open(tags_path, "w") as f:
        for tag in tags:
            f.write("%s\n" % (tag))
