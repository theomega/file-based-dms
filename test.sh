poetry run autoflake -r --in-place --remove-all-unused-imports --remove-unused-variables .
poetry run isort .
poetry run black .

poetry run coverage run --source='.' --omit '*/lib/*' -m unittest
poetry run coverage report
poetry run coverage html
