#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:tw=80:ts=2:sw=2:colorcolumn=81:smartindent:expandtab
# pylint: disable=missing-docstring

import datetime
import os
import subprocess
import tempfile
import unittest

import file_handling

# Some constants to make tests easier to write
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
TEXT1_FILE = os.path.join(CURRENT_DIR, "text1.pdf")
TEXT1 = """This is a first file

1

\x0c"""

TEXT2_FILE = os.path.join(CURRENT_DIR, "text2.pdf")
TEXT2 = """And this is a second file

1

\x0c"""

IMAGE1_FILE = os.path.join(CURRENT_DIR, "image1.pdf")
IMAGE1 = """Text File 1

\x0c"""


def get_pdf_text(filename):
    """Uses the pdftotext unix tool to extract the text from a pdf"""
    res = subprocess.run(
        ["pdftotext", filename, "-"], stdout=subprocess.PIPE, check=True
    )
    return res.stdout.decode("utf-8")


class TestFileName(unittest.TestCase):
    def test_simple(self):
        self.assertEqual(
            file_handling.get_filename(
                "Title",
                ["one", "two"],
                datetime.date(2018, 7, 6),
                date_format="%Y-%m-%d",
            ),
            "2018-07-06 - Title [one][two].pdf",
        )

    def test_index(self):
        self.assertEqual(
            file_handling.get_filename(
                "Title",
                ["one", "two"],
                datetime.date(2018, 7, 6),
                date_format="%Y-%m-%d",
                index=4,
            ),
            "2018-07-06 - Title [one][two] - 04.pdf",
        )

    def test_no_tag(self):
        self.assertEqual(
            file_handling.get_filename(
                "Title", [], datetime.date(2018, 7, 6), date_format="%Y-%m-%d"
            ),
            "2018-07-06 - Title.pdf",
        )


class TestSetup(unittest.TestCase):
    """Only makes suare that the pdftotext command works so the tests should
    work"""

    def test_text1(self):
        self.assertEqual(get_pdf_text(TEXT1_FILE), TEXT1)

    def test_text2(self):
        self.assertEqual(get_pdf_text(TEXT2_FILE), TEXT2)


class TestJoin(unittest.TestCase):
    def test_simple(self):
        """Joins two text-pdfs together and checks that the text of both is in
        the result"""
        files = [TEXT1_FILE, TEXT2_FILE]

        with tempfile.NamedTemporaryFile() as output_file:
            file_handling.join_files(input_files=files, output_file=output_file.name)

            self.assertEqual(get_pdf_text(output_file.name), TEXT1 + TEXT2)


class TestOCR(unittest.TestCase):
    def test_simple(self):
        """Runs OCR on a simple pdf which contains some text as an image and
        checks that the text was added to the output pdf"""

        with tempfile.NamedTemporaryFile() as output_file:
            file_handling.ocr_file(
                input_file=IMAGE1_FILE, output_file=output_file.name, language="eng"
            )

            self.assertEqual(get_pdf_text(output_file.name), IMAGE1)

    def test_text_existing(self):
        """Runs OCR on a simple pdf which already some text and checks that the
        text is still in the output pdf"""

        with tempfile.NamedTemporaryFile() as output_file:
            file_handling.ocr_file(
                input_file=TEXT1_FILE, output_file=output_file.name, language="eng"
            )

            self.assertEqual(get_pdf_text(output_file.name), TEXT1)
