#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:tw=80:ts=2:sw=2:colorcolumn=81:smartindent:expandtab
# pylint: disable=missing-docstring

import tempfile
import unittest

import tag_handling


class TestTagHandling(unittest.TestCase):
    def test_read_nonexisting(self):
        self.assertEqual(len(tag_handling.read_tags("/doesntexist")), 0)

    def test_write_read(self):
        tags = set(["a", "b", "something"])
        with tempfile.NamedTemporaryFile() as output_file:
            tag_handling.write_tags(output_file.name, tags)
            written_tags = tag_handling.read_tags(output_file.name)

            self.assertEqual(tags, written_tags)
