#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:tw=80:ts=2:sw=2:colorcolumn=81:nosmartindent:expandtab
"""Helper functions for the file-based-dms, mainly around prompting"""

import argparse
import datetime
import logging
import os
import readline
import string

import file_handling

LOGGER = logging.getLogger("utils")


def validate_file(file_name):
    """Validates that the provided filename is a valid and readable file.
    If not, throws an argparse ArgumenTypeError. This function is meant
    to be used as an argparse validator.

    Args:
      file_name (string): The filename to check for readability
    """

    if not os.path.isfile(file_name):
        msg = "%s is not a readable file" % (file_name)
        raise argparse.ArgumentTypeError(msg)

    return file_name


def get_validate_date(date_format):
    """Returns a validation function for the date according to the provided
    date_format string.

    Args:
      date_format: A format string for the date to validate against

    Returns:
      function: A validation function which takes a string and returns
                a boolean depending if the provided string is a valid date.
    """

    def validate_date(user_input):
        LOGGER.debug("Validating date %s against %s", user_input, date_format)
        try:
            datetime.datetime.strptime(user_input, date_format)
            return True
        except ValueError:
            return False

    return validate_date


def get_validate_title():
    """Returns a validation function for the document title.

    Returns:
      function: A validation function which takes a string and returns
                a boolean depending if the provided string is a valid document
                title.
    """

    allowed_chars = file_handling.get_valid_filename_chars()

    def validate_title(user_input):
        LOGGER.debug("Validating title %s", user_input)
        if len(user_input) < 2:
            return False
        return all([c in allowed_chars for c in user_input])

    return validate_title


def get_validate_tags():
    """Returns a validation function for the document tags.

    Returns:
      function: A validation function which takes a string and returns
                a boolean depending if the provided string is a valid list of
                tags for a document.
    """

    allowed_chars = string.ascii_letters + string.digits

    def validate_tags(user_input):
        LOGGER.debug("Validating tags %s", user_input)
        if not user_input:
            # No tags is OK
            return True

        for tag in user_input.split(" "):
            if not all([c in allowed_chars for c in tag]):
                return False
        return True

    return validate_tags


def get_completer(completion_options):
    """Constructs a readline completion function. For internal use only.

    Args:
      completion_options (list): A list of options which should be offered
                                 for tab completion.

    Returns:
      function: A function which can be used as readline completion function.
    """

    LOGGER.debug("Returning Completer for %s", completion_options)

    def complete(text, state):
        states = [
            x + " " for x in completion_options if x.lower().startswith(text.lower())
        ] + [None]
        LOGGER.debug("Returning states in completer %s", states)
        return states[state]

    return complete


def get_prompt(
    prompt, history_file=None, completion_options=None, validation=None, default=None
):
    """Interactive prompt the user for input. Potentially offers the possibility
    to validate the input and to offer completion using readline. Also stores
    the previous answers in a file and offers them the next time.

    Args:
      prompt (str): The literal prompt to display to the user.
      history_file (str): Full path to a file where the history should be read
                          from and stored to. The file doesn't have to exist.
                          Can be None to disable history support.
      completion_options (list): If tab-completion should be enabled, provide
                                 a list of options here.
      validation (function): To enable validation, provide a function here which
                             accepts the user input as first and only string
                             parameter. The function should return a boolean:
                             true if the input is valid.
      default (str): The default to offer to the user

    Returns:
      str: The information the user entered
    """

    if history_file:
        history_path = os.path.expanduser(history_file)
        history_dir = os.path.dirname(history_path)
        if not os.path.exists(history_dir):
            os.makedirs(history_dir)

        if os.path.exists(history_path):
            readline.read_history_file(history_path)
    else:
        history_path = None

    while True:
        if completion_options is not None:
            readline.set_completer(get_completer(completion_options))
            readline.parse_and_bind("tab: complete")
            readline.parse_and_bind('bind ^I rl_complete')
        else:
            readline.parse_and_bind("tab: None")

        if default:
            LOGGER.debug("Providing pre %s", default)

            def hook():
                readline.insert_text(default)
                readline.redisplay()

            readline.set_pre_input_hook(hook)
        else:
            readline.set_pre_input_hook()
        user_input = input(prompt)
        user_input = user_input.strip()

        LOGGER.debug("Default=%s, user_input=%s", default, user_input)

        if not validation or validation(user_input):
            if history_path:
                readline.write_history_file(history_path)
            readline.clear_history()

            LOGGER.debug("Returning %s", user_input)
            return user_input

        if user_input != "":
            default = user_input
